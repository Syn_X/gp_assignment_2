#pragma once

#include <application/directx_app.h>
#include <input/keyboard.h>
#include <camera/spherical_camera.h>

namespace xtest {
namespace demo {

	class LightsDemoApp : public application::DirectxApp, public input::MouseListener, public input::KeyboardListener
	{
	public:

		struct VertexIn {
			DirectX::XMFLOAT3 pos;
			float _explicit_pad_1;
			DirectX::XMFLOAT3 normal;
			float _explicit_pad_2;
		};

		struct Material
		{
			DirectX::XMFLOAT4 ambient;
			DirectX::XMFLOAT4 diffuse;
			DirectX::XMFLOAT4 specular;
		};

		struct DirectionalLight
		{
			DirectX::XMFLOAT4 ambient;
			DirectX::XMFLOAT4 diffuse;
			DirectX::XMFLOAT4 specular;
			DirectX::XMFLOAT3 dirW;
			float _explicit_pad_;
		};

		struct PointLight
		{
			DirectX::XMFLOAT4 ambient;
			DirectX::XMFLOAT4 diffuse;
			DirectX::XMFLOAT4 specular;
			DirectX::XMFLOAT3 posW;
			float range;
			DirectX::XMFLOAT3 attenuation;
			float _explicit_pad_;
		};

		struct SpotLight
		{
			DirectX::XMFLOAT4 ambient;
			DirectX::XMFLOAT4 diffuse;
			DirectX::XMFLOAT4 specular;
			DirectX::XMFLOAT3 posW;
			float range;
			DirectX::XMFLOAT3 dirW;
			float spot;
			DirectX::XMFLOAT3 attenuation;
			float _explicit_pad_;
		};

		struct PerObjectCB
		{
			DirectX::XMFLOAT4X4 W;
			DirectX::XMFLOAT4X4 W_inverseTranspose;
			DirectX::XMFLOAT4X4 WVP;
			Material material;
		};

		struct PerFrameCB
		{
			DirectionalLight dirLight;
			PointLight pointLight;
			SpotLight spotLight;
			DirectX::XMFLOAT3 eyePosW;
			float _explicit_pad_;
		};

		struct alignas (16) RarelyChangedCB
		{
			int useDirLight;
			int usePointLight;
			int useSpotLight;
			int _explicit_pad_;
		};

		LightsDemoApp(HINSTANCE instance, const application::WindowSettings& windowSettings, const application::DirectxSettings& directxSettings, uint32 fps = 60);
		~LightsDemoApp();

		LightsDemoApp(LightsDemoApp&&) = delete;
		LightsDemoApp(const LightsDemoApp&) = delete;
		LightsDemoApp& operator=(LightsDemoApp&&) = delete;
		LightsDemoApp& operator=(const LightsDemoApp&) = delete;


		virtual void Init() override;
		virtual void OnResized() override;
		virtual void UpdateScene(float deltaSeconds) override;
		virtual void RenderScene() override;

		virtual void OnWheelScroll(input::ScrollStatus scroll) override;
		virtual void OnMouseMove(const DirectX::XMINT2& movement, const DirectX::XMINT2& currentPos) override;
		virtual void OnKeyStatusChange(input::Key key, const input::KeyStatus& status) override;

	private:

		void InitMatrices();
		void InitShaders();
		void InitBuffers();
		void InitRasterizerState();

		void InitPlaneBuffers();
		void InitBoxBuffers();
		void InitSphereBuffers();
		void InitTorusBuffers();
		void InitCrateBuffers();
		void InitLightBuffers();
		void InitLightsValues();
		void InitRarelyChangedBuffer();

		void CreateVertexBuffer(UINT size, VertexIn* vertices, ID3D11Buffer** buffer);
		void CreateIndexBuffer(UINT size, uint32* indices, ID3D11Buffer** buffer);
		void CreateConstantBuffer(ID3D11Buffer** buffer);

		void UpdateObject(DirectX::XMFLOAT4X4* worldMatrix, ID3D11Buffer* constantBuffer, Material* material);
		void UpdateComposedObject(DirectX::XMFLOAT4X4* worldMatrix, std::vector<Microsoft::WRL::ComPtr<ID3D11Buffer>>* constantBuffers, std::vector<Material>* materials);
		void UpdatePerFrameConstantBuffer();
		void UpdateRarelyChangedConstantBuffer();


		DirectX::XMFLOAT4X4 m_viewMatrix;
		DirectX::XMFLOAT4X4 m_projectionMatrix;
		DirectX::XMFLOAT4X4 m_boxWorldMatrix;
		DirectX::XMFLOAT4X4 m_sphereWorldMatrix;
		DirectX::XMFLOAT4X4 m_planeWorldMatrix;
		DirectX::XMFLOAT4X4 m_torusWorldMatrix;
		DirectX::XMFLOAT4X4 m_crateWorldMatrix;

		camera::SphericalCamera m_camera;

		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer_Sphere;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer_Plane;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer_Box;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer_Torus;

		Microsoft::WRL::ComPtr<ID3D11Buffer> m_indexBuffer_Sphere;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_indexBuffer_Plane;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_indexBuffer_Box;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_indexBuffer_Torus;

		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vsConstantBufferPerObject_Sphere;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vsConstantBufferPerObject_Plane;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vsConstantBufferPerObject_Box;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vsConstantBufferPerObject_Torus;

		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vsConstantBufferPerFrame;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vsConstantBufferRarelyChanged;

		Microsoft::WRL::ComPtr<ID3D11VertexShader> m_vertexShader;
		Microsoft::WRL::ComPtr<ID3D11PixelShader> m_pixelShader;
		Microsoft::WRL::ComPtr<ID3D11InputLayout> m_inputLayout;
		Microsoft::WRL::ComPtr<ID3D11RasterizerState> m_rasterizerState;

		uint32 boxIndicesNum = 0;
		uint32 sphereIndicesNum = 0;
		uint32 planeIndicesNum = 0;
		uint32 torusIndicesNum = 0;
		uint32 crateIndicesNum = 0;

		std::vector<Microsoft::WRL::ComPtr<ID3D11Buffer>> m_crate_vBuffers;
		std::vector<Microsoft::WRL::ComPtr<ID3D11Buffer>> m_crate_iBuffers;
		std::vector<Microsoft::WRL::ComPtr<ID3D11Buffer>> m_crate_cBuffers;
		std::vector<uint32> m_crate_IndicesCount;
		std::vector<Material> m_crate_Materials;

		bool updateActiveLights = true;
		bool useDirLight = true;
		bool usePointLight = true;
		bool useSpotLight = true;

		DirectionalLight dirL = DirectionalLight();

		PointLight pointL = PointLight();

		SpotLight spotL = SpotLight();

		/*
		* Materials
		*/
		Material silverMaterial =
		{
			DirectX::XMFLOAT4(0.19225f,	0.19225f,	0.19225f,	1.0f),	//ambient
			DirectX::XMFLOAT4(0.50754f,	0.50754f,	0.50754f,	1.0f),	//diffuse
			DirectX::XMFLOAT4(0.508273f, 0.508273f,	0.508273f,	100.0f)	//specular
		};

		Material goldMaterial
		{
			DirectX::XMFLOAT4(0.24725f,	0.1995f,	0.0745f,		1.0f),
			DirectX::XMFLOAT4(0.75164f,	0.60648f,	0.22648f,	1.0f),
			DirectX::XMFLOAT4(0.628281f, 0.555802f,	0.366065f,	500.0f),
		};

		Material chromeMaterial
		{
			DirectX::XMFLOAT4(0.25f,		0.25f,		0.25f,		1.0f),
			DirectX::XMFLOAT4(0.4f,			0.4f,		0.4f,		1.0f),
			DirectX::XMFLOAT4(0.774597f,	0.774597f,	0.774597f,	500.f)
		};


		Material rubyMaterial
		{
			DirectX::XMFLOAT4(0.1745f,	0.01175f,	0.01175f,	1.0f),
			DirectX::XMFLOAT4(0.61424f,	0.04136f,	0.04136f,	1.0f),
			DirectX::XMFLOAT4(0.727811f, 0.626959f,	0.626959f,	200.0f)
		};

		Material greenMaterial
		{
			DirectX::XMFLOAT4(0.07568f,	0.61424f, 0.07568f, 1.0f),
			DirectX::XMFLOAT4(0.07568f, 0.61424f, 0.07568f, 1.0f),
			DirectX::XMFLOAT4(0.07568f, 0.61424f, 0.27568f, 1.0f)
		};

		Material emeraldMaterial
		{
			DirectX::XMFLOAT4(0.0215f,	0.1745f,	0.0215f,	1.0f),
			DirectX::XMFLOAT4(0.07568f, 0.61424f,	0.07568f,	1.0f),
			DirectX::XMFLOAT4(0.633f,	0.727811f,	0.633f,		150.0f)
		};


	};

} // demo
} // xtest



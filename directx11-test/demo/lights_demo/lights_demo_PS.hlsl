struct Material
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
};

struct DirectionalLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float3 dirW;
};

struct PointLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float3 posW;
	float range;
	float3 attenuation;
};

struct SpotLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float3 posW;
	float range;
	float3 dirW;
	float spot;
	float3 attenuation;
};

cbuffer PerObjectCB : register(b0)
{
	float4x4 W;
	float4x4 W_inverseTranspose;
	float4x4 WVP;
	Material material;
}

cbuffer PerFrameCB : register(b1)
{
	DirectionalLight dirLight;
	PointLight pointLight;
	SpotLight spotLight;
	float3 eyePosW;
}

cbuffer RarelyChangedCB : register(b2)
{
	int useDirLight;
	int usePointLight;
	int useSpotLight;
}

struct VertexOut
{
	float4 posH : SV_POSITION;
	float3 normalW : W_NORMAL;
	float3 posW : W_POSITION;
};



void DirectionalLightContribution(Material mat, DirectionalLight light, float3 normalW, float3 toEyeW, out float4 ambient, out float4 diffuse, out float4 specular)
{
	ambient = float4(0.f, 0.f, 0.f, 0.f);
	diffuse = float4(0.f, 0.f, 0.f, 0.f);
	specular = float4(0.f, 0.f, 0.f, 0.f);
	
	ambient += mat.ambient * light.ambient;
	
	float3 toLightW = -light.dirW;
	float Kd = dot(toLightW, normalW);
	
	[flatten]
	if (Kd > 0.f)
	{
		diffuse += Kd * mat.diffuse * light.diffuse;
		
		float3 halfVectorW = normalize(toLightW + toEyeW);
		float Ks = pow(max(dot(halfVectorW, normalW), 0.f), mat.specular.w);
		specular += Ks * mat.specular * light.specular;
	}

}

void PointLightContribution(Material mat, PointLight light, float3 posW, float3 normalW, float3 toEyeW, out float4 ambient, out float4 diffuse, out float4 specular)
{
	ambient = float4(0.f, 0.f, 0.f, 0.f);
	diffuse = float4(0.f, 0.f, 0.f, 0.f);
	specular = float4(0.f, 0.f, 0.f, 0.f);
	
	
	float3 toLightW = light.posW - posW;
	float dist = length(toLightW);
	
	if (dist < light.range)
	{
		toLightW /= dist; // normalize toLighW
		
		float attenuation = (light.attenuation.x + light.attenuation.y * dist + light.attenuation.z * dist * dist);
		if (attenuation != 0)
		{
			attenuation = 1.f / attenuation;
		}
		else
		{
			attenuation = 1.f;
		}
		
		ambient += mat.ambient * light.ambient * attenuation;

		float Kd = dot(toLightW, normalW);
	
		[flatten]
		if (Kd > 0.f)
		{
			diffuse += Kd * mat.diffuse * light.diffuse * attenuation;
		
			float3 halfVectorW = normalize(toLightW + toEyeW);
			float Ks = pow(max(dot(halfVectorW, normalW), 0.f), mat.specular.w);
			specular += Ks * mat.specular * light.specular * attenuation;
		}
	}
}

void SpotLightContribution(Material mat, SpotLight light, float3 posW, float3 normalW, float3 toEyeW, out float4 ambient, out float4 diffuse, out float4 specular)
{
	ambient = float4(0.f, 0.f, 0.f, 0.f);
	diffuse = float4(0.f, 0.f, 0.f, 0.f);
	specular = float4(0.f, 0.f, 0.f, 0.f);
	
	float3 toLightW = light.posW - posW;
	float dist = length(toLightW);
	
	if (dist < light.range)
	{
		toLightW /= dist;

		float attenuation = (light.attenuation.x + light.attenuation.y * dist + light.attenuation.z * dist * dist);
		if (attenuation != 0)
		{
			attenuation = 1.f / attenuation;
		}
		else
		{
			attenuation = 1.f;
		}

		float Kspot = pow(max(dot(-toLightW, light.dirW), 0.f), light.spot);

		ambient += mat.ambient * light.ambient * attenuation * Kspot;

		float Kd = dot(toLightW, normalW);
	
		[flatten]
		if (Kd > 0.f)
		{

			diffuse += Kd * mat.diffuse * light.diffuse * attenuation * Kspot;
		
			float3 halfVectorW = normalize(toLightW + toEyeW);
			float Ks = pow(max(dot(halfVectorW, normalW), 0.f), mat.specular.w);
			
			
			specular += Ks * mat.specular * light.specular * attenuation * Kspot;
		}
	}
}

float4 main(VertexOut pin) : SV_TARGET
{
	pin.normalW = normalize(pin.normalW);
	
	float3 toEyeW = normalize(eyePosW - pin.posW);
	
	float4 totalAmbient = float4(0.f, 0.f, 0.f, 0.f);
	float4 totalDiffuse = float4(0.f, 0.f, 0.f, 0.f);
	float4 totalSpecular = float4(0.f, 0.f, 0.f, 0.f);
	
	float4 ambient;
	float4 diffuse;
	float4 specular;

	if (useDirLight)
	{
		DirectionalLightContribution(material, dirLight, pin.normalW, toEyeW, ambient, diffuse, specular);
		totalAmbient += ambient;
		totalDiffuse += diffuse;
		totalSpecular += specular;
	}

	if (usePointLight)
	{
		PointLightContribution(material, pointLight, pin.posW, pin.normalW, toEyeW, ambient, diffuse, specular);
		totalAmbient += ambient;
		totalDiffuse += diffuse;
		totalSpecular += specular;
	}

	if (useSpotLight)
	{
		SpotLightContribution(material, spotLight, pin.posW, pin.normalW, toEyeW, ambient, diffuse, specular);
		totalAmbient += ambient;
		totalDiffuse += diffuse;
		totalSpecular += specular;
	}

	float4 finalColor = totalAmbient + totalDiffuse + totalSpecular;
	finalColor.a = totalDiffuse.a;
	
	return finalColor;

}
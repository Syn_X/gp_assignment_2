#include "stdafx.h"
#include "lights_demo_app.h"
#include <file/file_utils.h>
#include <math/math_utils.h>
#include <service/locator.h>
#include <mesh/mesh_generator.h>

using namespace DirectX;
using namespace xtest;

using xtest::demo::LightsDemoApp;
using Microsoft::WRL::ComPtr;

LightsDemoApp::LightsDemoApp(HINSTANCE instance,
	const application::WindowSettings& windowSettings,
	const application::DirectxSettings& directxSettings,
	uint32 fps /*=60*/)
	: application::DirectxApp(instance, windowSettings, directxSettings, fps)
	, m_vertexShader(nullptr)
	, m_pixelShader(nullptr)
	, m_inputLayout(nullptr)
	, m_camera(math::ToRadians(90.f), math::ToRadians(30.f), 10.f, { 0.f, 0.f, 0.f }, { 0.f, 1.f, 0.f }, { math::ToRadians(5.f), math::ToRadians(175.f) }, { 3.f, 25.f })
{}


LightsDemoApp::~LightsDemoApp()
{}

void LightsDemoApp::Init()
{
	application::DirectxApp::Init();
	m_d3dAnnotation->BeginEvent(L"init-demo");

	InitMatrices();
	InitShaders();
	InitBuffers();
	InitRasterizerState();

	service::Locator::GetMouse()->AddListener(this);
	service::Locator::GetKeyboard()->AddListener(this, { input::Key::F, input::Key::D, input::Key::P, input::Key::S });

	m_d3dAnnotation->EndEvent();
}

void LightsDemoApp::InitMatrices()
{
	// world matrices
	XMStoreFloat4x4(&m_boxWorldMatrix, XMMatrixIdentity());
	XMStoreFloat4x4(&m_planeWorldMatrix, XMMatrixIdentity());
	XMStoreFloat4x4(&m_sphereWorldMatrix, XMMatrixIdentity());
	XMStoreFloat4x4(&m_torusWorldMatrix, XMMatrixIdentity());
	XMStoreFloat4x4(&m_crateWorldMatrix, XMMatrixIdentity());

	// view matrix
	XMStoreFloat4x4(&m_viewMatrix, m_camera.GetViewMatrix());

	// projection matrix
	{
		XMMATRIX P = XMMatrixPerspectiveFovLH(math::ToRadians(45.f), AspectRatio(), 0.1f, 1000.f);
		XMStoreFloat4x4(&m_projectionMatrix, P);
	}
}

void LightsDemoApp::InitShaders()
{
	// read pre-compiled shaders' bytecode
	std::future<file::BinaryFile> psByteCodeFuture = file::ReadBinaryFile(std::wstring(GetRootDir()).append(L"\\lights_demo_PS.cso"));
	std::future<file::BinaryFile> vsByteCodeFuture = file::ReadBinaryFile(std::wstring(GetRootDir()).append(L"\\lights_demo_VS.cso"));

	// future.get() can be called only once
	file::BinaryFile vsByteCode = vsByteCodeFuture.get();
	file::BinaryFile psByteCode = psByteCodeFuture.get();
	XTEST_D3D_CHECK(m_d3dDevice->CreateVertexShader(vsByteCode.Data(), vsByteCode.ByteSize(), nullptr, &m_vertexShader));
	XTEST_D3D_CHECK(m_d3dDevice->CreatePixelShader(psByteCode.Data(), psByteCode.ByteSize(), nullptr, &m_pixelShader));

	D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexIn, normal), D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	XTEST_D3D_CHECK(m_d3dDevice->CreateInputLayout(vertexDesc, 2, vsByteCode.Data(), vsByteCode.ByteSize(), &m_inputLayout));
}

void LightsDemoApp::InitBuffers()
{
	InitPlaneBuffers();
	InitBoxBuffers();
	InitSphereBuffers();
	InitTorusBuffers();

	InitCrateBuffers();

	InitLightBuffers();
	InitLightsValues();

	InitRarelyChangedBuffer();
}

void LightsDemoApp::InitLightBuffers()
{
	D3D11_BUFFER_DESC vsConstantBufferDesc;
	vsConstantBufferDesc.Usage = D3D11_USAGE_DYNAMIC; // this buffer needs to be updated every frame
	vsConstantBufferDesc.ByteWidth = sizeof(PerFrameCB);
	vsConstantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	vsConstantBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vsConstantBufferDesc.MiscFlags = 0;
	vsConstantBufferDesc.StructureByteStride = 0;
	XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&vsConstantBufferDesc, nullptr, &m_vsConstantBufferPerFrame));
}

void LightsDemoApp::InitLightsValues() 
{
	// White directional light
	dirL._explicit_pad_ = 0.f;
	dirL.ambient = XMFLOAT4(	0.5f, 0.5f, 0.5f, 1.f);
	dirL.diffuse = XMFLOAT4(	0.3f, 0.3f, 0.3f, 1.f);
	dirL.specular = XMFLOAT4(	0.1f, 0.1f, 0.1f, 1.f);
	dirL.dirW = XMFLOAT3(0.f, -1.f, 0.f);

	// Blue point light
	pointL._explicit_pad_ = 0.f;
	pointL.ambient = XMFLOAT4(0.f, 0.f, 0.5f, 1.f);
	pointL.diffuse = XMFLOAT4(0.f, 0.f, 0.5f, 1.f);
	pointL.specular = XMFLOAT4(0.f, 0.f, 0.5f, 1.f);
	pointL.attenuation = XMFLOAT3(0.05f, 0.05f, 0.05f);
	pointL.range = 10.f;
	pointL.posW = XMFLOAT3(0.0f, -1.0f, 0.0f);

	// Red spot light
	spotL._explicit_pad_ = 0.f;
	spotL.ambient = XMFLOAT4(0.9f, 0.f, 0.f, 1.f);
	spotL.diffuse = XMFLOAT4(0.9f, 0.f, 0.f, 1.f);
	spotL.specular = XMFLOAT4(0.9f, 0.f, 0.f, 1.f);
	spotL.attenuation = XMFLOAT3(0.2f, 0.2f, 0.2f);
	spotL.range = 200.f;
	spotL.posW = XMFLOAT3(6.0f, .0f, 4.0f);
	spotL.dirW = XMFLOAT3(-1.0f, .0f, .0f);
	spotL.spot = 5.f;
}

void LightsDemoApp::InitRarelyChangedBuffer() 
{
	D3D11_BUFFER_DESC vsConstantBufferDesc;
	vsConstantBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	vsConstantBufferDesc.ByteWidth = sizeof(RarelyChangedCB);
	vsConstantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	vsConstantBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vsConstantBufferDesc.MiscFlags = 0;
	vsConstantBufferDesc.StructureByteStride = 0;
	XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&vsConstantBufferDesc, nullptr, &m_vsConstantBufferRarelyChanged));
}

void LightsDemoApp::InitRasterizerState()
{
	// rasterizer state
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	rasterizerDesc.FrontCounterClockwise = false;
	rasterizerDesc.DepthClipEnable = true;

	m_d3dDevice->CreateRasterizerState(&rasterizerDesc, &m_rasterizerState);
}

void LightsDemoApp::OnResized()
{
	application::DirectxApp::OnResized();
	//update the projection matrix with the new aspect ratio
	XMMATRIX P = XMMatrixPerspectiveFovLH(math::ToRadians(45.f), AspectRatio(), 1.f, 1000.f);
	XMStoreFloat4x4(&m_projectionMatrix, P);
}

void LightsDemoApp::UpdateScene(float deltaSeconds)
{
	XTEST_UNUSED_VAR(deltaSeconds);

	m_d3dAnnotation->BeginEvent(L"update-constant-buffer");

	// load the constant buffer data in the gpu

	//// BOX
	UpdateObject(&m_boxWorldMatrix, m_vsConstantBufferPerObject_Box.Get(), &goldMaterial);

	// SPHERE
	UpdateObject(&m_sphereWorldMatrix, m_vsConstantBufferPerObject_Sphere.Get(), &emeraldMaterial);

	// TORUS
	UpdateObject(&m_torusWorldMatrix, m_vsConstantBufferPerObject_Torus.Get(), &rubyMaterial);

	// PLANE
	UpdateObject(&m_planeWorldMatrix, m_vsConstantBufferPerObject_Plane.Get(), &chromeMaterial);


	// CRATE
	//std::vector<Material> crateMaterials = { rubyMaterial, emeraldMaterial, goldMaterial, chromeMaterial };
	UpdateComposedObject(&m_crateWorldMatrix, &m_crate_cBuffers, &m_crate_Materials);

	// LIGHT
	UpdatePerFrameConstantBuffer();

	// LIGHT ON/OFF
	if (updateActiveLights) 
	{
		updateActiveLights = false;
		UpdateRarelyChangedConstantBuffer();
	}

	m_d3dAnnotation->EndEvent();
}

void LightsDemoApp::RenderScene()
{
	m_d3dAnnotation->BeginEvent(L"render-scene");

	// clear the frame
	m_d3dContext->ClearDepthStencilView(m_depthBufferView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.f, 0);
	m_d3dContext->ClearRenderTargetView(m_backBufferView.Get(), DirectX::Colors::Black);

	// set the shaders and the input layout
	m_d3dContext->RSSetState(m_rasterizerState.Get());
	m_d3dContext->IASetInputLayout(m_inputLayout.Get());
	m_d3dContext->VSSetShader(m_vertexShader.Get(), nullptr, 0);
	m_d3dContext->PSSetShader(m_pixelShader.Get(), nullptr, 0);

	{
		/*
		* BOX
		*/
		// bind the constant data to the vertex shader
		UINT bufferRegister = 0; // PerObjectCB was defined as register 0 inside the vertex shader file thanks to syntax (b0)
		m_d3dContext->VSSetConstantBuffers(bufferRegister, 1, m_vsConstantBufferPerObject_Box.GetAddressOf());

		// bind the constant data to the pixel shader
		ID3D11Buffer* pixelShaderConstantBuffers[3] = { m_vsConstantBufferPerObject_Box.Get(), m_vsConstantBufferPerFrame.Get(), m_vsConstantBufferRarelyChanged.Get() };
		m_d3dContext->PSSetConstantBuffers(bufferRegister, 3, pixelShaderConstantBuffers);

		// set what to draw
		UINT stride = sizeof(VertexIn);
		UINT offset = 0;
		m_d3dContext->IASetVertexBuffers(0, 1, m_vertexBuffer_Box.GetAddressOf(), &stride, &offset);
		m_d3dContext->IASetIndexBuffer(m_indexBuffer_Box.Get(), DXGI_FORMAT_R32_UINT, 0);
		m_d3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		// draw and present the frame
		m_d3dContext->DrawIndexed(boxIndicesNum, 0, 0);
	}

	{
		/*
		* SPHERE
		*/
		// bind the constant data to the vertex shader
		UINT bufferRegister = 0; // PerObjectCB was defined as register 0 inside the vertex shader file thanks to syntax (b0)
		m_d3dContext->VSSetConstantBuffers(bufferRegister, 1, m_vsConstantBufferPerObject_Sphere.GetAddressOf());

		// bind the constant data to the pixel shader
		ID3D11Buffer* pixelShaderConstantBuffers[3] = { m_vsConstantBufferPerObject_Sphere.Get(), m_vsConstantBufferPerFrame.Get(), m_vsConstantBufferRarelyChanged.Get() };
		m_d3dContext->PSSetConstantBuffers(bufferRegister, 3, pixelShaderConstantBuffers);

		// set what to draw
		UINT stride = sizeof(VertexIn);
		UINT offset = 0;
		m_d3dContext->IASetVertexBuffers(0, 1, m_vertexBuffer_Sphere.GetAddressOf(), &stride, &offset);
		m_d3dContext->IASetIndexBuffer(m_indexBuffer_Sphere.Get(), DXGI_FORMAT_R32_UINT, 0);
		m_d3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		// draw and present the frame
		m_d3dContext->DrawIndexed(sphereIndicesNum, 0, 0);
	}

	{
		/*
		* PLANE
		*/
		// bind the constant data to the vertex shader
		UINT bufferRegister = 0; // PerObjectCB was defined as register 0 inside the vertex shader file thanks to syntax (b0)
		m_d3dContext->VSSetConstantBuffers(bufferRegister, 1, m_vsConstantBufferPerObject_Plane.GetAddressOf());

		// bind the constant data to the pixel shader
		ID3D11Buffer* pixelShaderConstantBuffers[3] = { m_vsConstantBufferPerObject_Plane.Get(), m_vsConstantBufferPerFrame.Get(), m_vsConstantBufferRarelyChanged.Get() };
		m_d3dContext->PSSetConstantBuffers(bufferRegister, 3, pixelShaderConstantBuffers);

		// set what to draw
		UINT stride = sizeof(VertexIn);
		UINT offset = 0;
		m_d3dContext->IASetVertexBuffers(0, 1, m_vertexBuffer_Plane.GetAddressOf(), &stride, &offset);
		m_d3dContext->IASetIndexBuffer(m_indexBuffer_Plane.Get(), DXGI_FORMAT_R32_UINT, 0);
		m_d3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		// draw and present the frame
		m_d3dContext->DrawIndexed(planeIndicesNum, 0, 0);
	}

	{
		/*
		* TORUS
		*/
		// bind the constant data to the vertex shader
		UINT bufferRegister = 0; // PerObjectCB was defined as register 0 inside the vertex shader file thanks to syntax (b0)
		m_d3dContext->VSSetConstantBuffers(bufferRegister, 1, m_vsConstantBufferPerObject_Torus.GetAddressOf());

		// bind the constant data to the pixel shader
		ID3D11Buffer* pixelShaderConstantBuffers[3] = { m_vsConstantBufferPerObject_Torus.Get(), m_vsConstantBufferPerFrame.Get(), m_vsConstantBufferRarelyChanged.Get() };
		m_d3dContext->PSSetConstantBuffers(bufferRegister, 3, pixelShaderConstantBuffers);

		// set what to draw
		UINT stride = sizeof(VertexIn);
		UINT offset = 0;
		m_d3dContext->IASetVertexBuffers(0, 1, m_vertexBuffer_Torus.GetAddressOf(), &stride, &offset);
		m_d3dContext->IASetIndexBuffer(m_indexBuffer_Torus.Get(), DXGI_FORMAT_R32_UINT, 0);
		m_d3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		// draw and present the frame
		m_d3dContext->DrawIndexed(torusIndicesNum, 0, 0);
	}

	{
		/*
		* CRATE
		*/
		for (int i = 0; i < m_crate_cBuffers.size(); i++)
		{
			// bind the constant data to the vertex shader
			UINT bufferRegister = 0; // PerObjectCB was defined as register 0 inside the vertex shader file thanks to syntax (b0)
			m_d3dContext->VSSetConstantBuffers(bufferRegister, 1, m_crate_cBuffers[i].GetAddressOf());

			// bind the constant data to the pixel shader
			ID3D11Buffer* pixelShaderConstantBuffers[3] = { m_crate_cBuffers[i].Get(), m_vsConstantBufferPerFrame.Get(), m_vsConstantBufferRarelyChanged.Get() };
			m_d3dContext->PSSetConstantBuffers(bufferRegister, 3, pixelShaderConstantBuffers);

			// set what to draw
			UINT stride = sizeof(VertexIn);
			UINT offset = 0;
			m_d3dContext->IASetVertexBuffers(0, 1, m_crate_vBuffers[i].GetAddressOf(), &stride, &offset);
			m_d3dContext->IASetIndexBuffer(m_crate_iBuffers[i].Get(), DXGI_FORMAT_R32_UINT, 0);
			m_d3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			// draw and present the frame
			m_d3dContext->DrawIndexed(m_crate_IndicesCount[i], 0, 0);
		}
	}

	XTEST_D3D_CHECK(m_swapChain->Present(0, 0));

	m_d3dAnnotation->EndEvent();
}

#pragma region Initialize buffers for objects


void LightsDemoApp::InitCrateBuffers()
{
	// get mesh from .gpf file
	const xtest::mesh::GPFMesh* mesh = (service::Locator::GetResourceLoader())->LoadGPFMesh(GetRootDir() + L"\\3d-objects\\crate\\crate.gpf");
	//const xtest::mesh::GPFMesh* mesh = (service::Locator::GetResourceLoader())->LoadGPFMesh(GetRootDir() + L"\\3d-objects\\rocks_dorama\\rocks_composition.gpf");
	//const xtest::mesh::GPFMesh* mesh = (service::Locator::GetResourceLoader())->LoadGPFMesh(GetRootDir() + L"\\3d-objects\\gdc_female\\gdc_female.gpf");
	//const xtest::mesh::GPFMesh* mesh = (service::Locator::GetResourceLoader())->LoadGPFMesh(GetRootDir() + L"\\3d-objects\\gdc_female\\gdc_female_posed.gpf");
	//const xtest::mesh::GPFMesh* mesh = (service::Locator::GetResourceLoader())->LoadGPFMesh(GetRootDir() + L"\\3d-objects\\gdc_female\\gdc_female_posed_2.gpf");

	// create vectors with buffers to render each crate's component
	for (auto iter = mesh->meshDescriptorMapByName.begin(); iter != mesh->meshDescriptorMapByName.end(); iter++) {

		std::vector<VertexIn> vertices;
		std::vector<uint32> indices;

		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vsConstantBufferPerObject_Crate;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_indexBuffer_Crate;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer_Crate;

		uint32 vCount = iter->second.vertexCount;
		uint32 vOff = iter->second.vertexOffset;

		Material m = {
			iter->second.material.ambient,
			iter->second.material.diffuse,
			iter->second.material.specular,
		};

		m_crate_Materials.push_back(m);

		for (uint32 i = 0; i < vCount; i++)
		{
			VertexIn v;
			v.pos = mesh->meshData.vertices[vOff + i].position;
			v._explicit_pad_1 = 0.f;
			v.normal = mesh->meshData.vertices[vOff + i].normal;
			v._explicit_pad_2 = 0.f;

			vertices.push_back(v);
		}

		uint32 iCount = iter->second.indexCount;
		uint32 iOff = iter->second.indexOffset;

		for (uint32 i = 0; i < iCount; i++)
		{
			indices.push_back(mesh->meshData.indices[iOff + i]);
		}

		m_crate_IndicesCount.push_back(iCount);

		CreateVertexBuffer(UINT(sizeof(VertexIn) * vertices.size()), &vertices[0], &m_vertexBuffer_Crate);
		CreateIndexBuffer(UINT(sizeof(uint32) * indices.size()), &indices[0], &m_indexBuffer_Crate);
		CreateConstantBuffer(&m_vsConstantBufferPerObject_Crate);

		m_crate_vBuffers.push_back(m_vertexBuffer_Crate.Get());
		m_crate_iBuffers.push_back(m_indexBuffer_Crate.Get());
		m_crate_cBuffers.push_back(m_vsConstantBufferPerObject_Crate.Get());
	}

	XMMATRIX translationMatrix = XMMatrixTranslation(0.0f, 0.0f, -2.0f);
	XMMATRIX rotationMatrix = XMMatrixIdentity();
	XMMATRIX scaleMatrix = XMMatrixScaling(1.0f, 1.0f, 1.0f);
	XMMATRIX W = scaleMatrix * rotationMatrix * translationMatrix;
	XMStoreFloat4x4(&m_crateWorldMatrix, W);
}

void LightsDemoApp::InitPlaneBuffers()
{
	using namespace xtest::mesh;

	MeshData mesh = GeneratePlane(30.f, 30.f, 2, 2);

	std::vector<VertexIn> vertices;
	for (int i = 0; i < mesh.vertices.size(); i++)
	{
		VertexIn v;
		v.pos = mesh.vertices[i].position;
		v._explicit_pad_1 = 0.f;
		v.normal = mesh.vertices[i].normal;
		v._explicit_pad_2 = 0.f;

		vertices.push_back(v);
	}

	planeIndicesNum = (uint32)mesh.indices.size();

	XMMATRIX translationMatrix = XMMatrixTranslation(.0f, .0f, .0f);
	XMMATRIX rotationMatrix = XMMatrixRotationY(XMConvertToRadians(45.0f));
	XMMATRIX scaleMatrix = XMMatrixScaling(1.0f, 1.0f, 1.0f);
	XMMATRIX W = scaleMatrix * rotationMatrix * translationMatrix;
	XMStoreFloat4x4(&m_planeWorldMatrix, W);

	CreateVertexBuffer(UINT(sizeof(VertexIn) * vertices.size()), &vertices[0], &m_vertexBuffer_Plane);
	CreateIndexBuffer(UINT(sizeof(uint32) * mesh.indices.size()), &mesh.indices[0], &m_indexBuffer_Plane);
	CreateConstantBuffer(&m_vsConstantBufferPerObject_Plane);
}

void LightsDemoApp::InitBoxBuffers()
{
	using namespace xtest::mesh;

	MeshData mesh = GenerateBox(1.f, 1.f, 1.f);

	std::vector<VertexIn> vertices;
	for (int i = 0; i < mesh.vertices.size(); i++)
	{
		VertexIn v;
		v.pos = mesh.vertices[i].position;
		v._explicit_pad_1 = 0.f;
		v.normal = mesh.vertices[i].normal;
		v._explicit_pad_2 = 0.f;

		vertices.push_back(v);
	}

	boxIndicesNum = (uint32)mesh.indices.size();

	XMMATRIX translationMatrix = XMMatrixTranslation(4.0f, 2.0f, 4.0f);
	XMMATRIX rotationMatrix = XMMatrixRotationX(XMConvertToRadians(30.0f));
	XMMATRIX scaleMatrix = XMMatrixScaling(2.0f, 2.0f, 2.0f);
	XMMATRIX W = scaleMatrix * rotationMatrix * translationMatrix;
	XMStoreFloat4x4(&m_boxWorldMatrix, W);

	CreateVertexBuffer(UINT(sizeof(VertexIn) * vertices.size()), &vertices[0], &m_vertexBuffer_Box);
	CreateIndexBuffer(UINT(sizeof(uint32) * mesh.indices.size()), &mesh.indices[0], &m_indexBuffer_Box);
	CreateConstantBuffer(&m_vsConstantBufferPerObject_Box);
}

void LightsDemoApp::InitSphereBuffers()
{
	using namespace xtest::mesh;

	MeshData mesh = GenerateSphere(1.5f, 50, 50);

	std::vector<VertexIn> vertices;
	for (int i = 0; i < mesh.vertices.size(); i++)
	{
		VertexIn v;
		v.pos = mesh.vertices[i].position;
		v._explicit_pad_1 = 0.f;
		v.normal = mesh.vertices[i].normal;
		v._explicit_pad_2 = 0.f;

		vertices.push_back(v);
	}

	sphereIndicesNum = (uint32)mesh.indices.size();

	XMMATRIX translationMatrix = XMMatrixTranslation(-4.0f, 3.0f, 0.0f);
	XMMATRIX rotationMatrix = XMMatrixRotationY(XMConvertToRadians(0.0f));
	XMMATRIX scaleMatrix = XMMatrixScaling(1.0f, 1.0f, 1.0f);
	XMMATRIX W = scaleMatrix * rotationMatrix * translationMatrix;
	XMStoreFloat4x4(&m_sphereWorldMatrix, W);

	CreateVertexBuffer(UINT(sizeof(VertexIn) * vertices.size()), &vertices[0], &m_vertexBuffer_Sphere);
	CreateIndexBuffer(UINT(sizeof(uint32) * mesh.indices.size()), &mesh.indices[0], &m_indexBuffer_Sphere);
	CreateConstantBuffer(&m_vsConstantBufferPerObject_Sphere);
}

void LightsDemoApp::InitTorusBuffers()
{
	using namespace xtest::mesh;

	MeshData mesh = GenerateTorus(1.f, 0.3f, 100, 100);

	std::vector<VertexIn> vertices;
	for (int i = 0; i < mesh.vertices.size(); i++)
	{
		VertexIn v;
		v.pos = mesh.vertices[i].position;
		v._explicit_pad_1 = 0.f;
		v.normal = mesh.vertices[i].normal;
		v._explicit_pad_2 = 0.f;

		vertices.push_back(v);
	}

	torusIndicesNum = (uint32)mesh.indices.size();

	XMMATRIX translationMatrix = XMMatrixTranslation(0.0f, 3.0f, 4.0f);
	XMMATRIX rotationMatrix = XMMatrixRotationX(XMConvertToRadians(30.0f)) * XMMatrixRotationY(XMConvertToRadians(30.0f));
	XMMATRIX scaleMatrix = XMMatrixIdentity();
	XMMATRIX W = scaleMatrix * rotationMatrix * translationMatrix;
	XMStoreFloat4x4(&m_torusWorldMatrix, W);

	CreateVertexBuffer(UINT(sizeof(VertexIn) * vertices.size()), &vertices[0], &m_vertexBuffer_Torus);
	CreateIndexBuffer(UINT(sizeof(uint32) * mesh.indices.size()), &mesh.indices[0], &m_indexBuffer_Torus);
	CreateConstantBuffer(&m_vsConstantBufferPerObject_Torus);
}


#pragma endregion 

#pragma region Create buffers utilities


void LightsDemoApp::CreateVertexBuffer(UINT size, VertexIn* vertices, ID3D11Buffer** buffer)
{
	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.ByteWidth = size;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexInitData;
	vertexInitData.pSysMem = vertices;
	XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&vertexBufferDesc, &vertexInitData, buffer));

}

void LightsDemoApp::CreateIndexBuffer(UINT size, uint32* indices, ID3D11Buffer** buffer)
{
	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.ByteWidth = size;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indexInitdata;
	indexInitdata.pSysMem = indices;
	XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&indexBufferDesc, &indexInitdata, buffer));
}

void LightsDemoApp::CreateConstantBuffer(ID3D11Buffer** buffer)
{
	D3D11_BUFFER_DESC vsConstantBufferDesc;
	vsConstantBufferDesc.Usage = D3D11_USAGE_DYNAMIC; // this buffer needs to be updated every frame
	vsConstantBufferDesc.ByteWidth = sizeof(PerObjectCB);
	vsConstantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	vsConstantBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vsConstantBufferDesc.MiscFlags = 0;
	vsConstantBufferDesc.StructureByteStride = 0;
	XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&vsConstantBufferDesc, nullptr, buffer));
}

#pragma endregion

#pragma region Update utilities

void LightsDemoApp::UpdateObject(XMFLOAT4X4* worldMatrix, ID3D11Buffer* constantBuffer, Material* material)
{
	XMMATRIX W = XMLoadFloat4x4(worldMatrix);
	XMStoreFloat4x4(worldMatrix, W);

	// create the model-view-projection matrix
	XMMATRIX V = m_camera.GetViewMatrix();
	XMStoreFloat4x4(&m_viewMatrix, V);

	// create projection matrix
	XMMATRIX P = XMLoadFloat4x4(&m_projectionMatrix);
	XMMATRIX WVP = W * V * P;
	XMMATRIX W_inverseTranspose = XMMatrixTranspose(XMMatrixInverse(nullptr, W));

	// matrices must be transposed since HLSL use column-major ordering.
	WVP = XMMatrixTranspose(WVP);
	W = XMMatrixTranspose(W);
	W_inverseTranspose = XMMatrixTranspose(W_inverseTranspose);

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	// PerObjectCB
	// disable gpu access
	XTEST_D3D_CHECK(m_d3dContext->Map(constantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
	PerObjectCB* constantBufferData = static_cast<PerObjectCB*>(mappedResource.pData);

	//update the data
	XMStoreFloat4x4(&constantBufferData->WVP, WVP);
	XMStoreFloat4x4(&constantBufferData->W, W);
	XMStoreFloat4x4(&constantBufferData->W_inverseTranspose, W_inverseTranspose);

	constantBufferData->material = *material;

	// enable gpu access
	m_d3dContext->Unmap(constantBuffer, 0);
}

void LightsDemoApp::UpdateComposedObject(DirectX::XMFLOAT4X4* worldMatrix, std::vector<ComPtr<ID3D11Buffer>>* constantBuffers, std::vector<Material>* materials)
{
	XMMATRIX W = XMLoadFloat4x4(worldMatrix);
	XMStoreFloat4x4(worldMatrix, W);

	// create the model-view-projection matrix
	XMMATRIX V = m_camera.GetViewMatrix();
	XMStoreFloat4x4(&m_viewMatrix, V);

	// create projection matrix
	XMMATRIX P = XMLoadFloat4x4(&m_projectionMatrix);
	XMMATRIX WVP = W * V * P;
	XMMATRIX W_inverseTranspose = XMMatrixTranspose(XMMatrixInverse(nullptr, W));

	// matrices must be transposed since HLSL use column-major ordering.
	WVP = XMMatrixTranspose(WVP);
	W = XMMatrixTranspose(W);
	W_inverseTranspose = XMMatrixTranspose(W_inverseTranspose);

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	for (int i = 0; i < constantBuffers->size(); i++)
	{
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

		// PerObjectCB
		// disable gpu access
		XTEST_D3D_CHECK(m_d3dContext->Map((*constantBuffers)[i].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
		PerObjectCB* constantBufferData = static_cast<PerObjectCB*>(mappedResource.pData);

		//update the data
		XMStoreFloat4x4(&constantBufferData->WVP, WVP);
		XMStoreFloat4x4(&constantBufferData->W, W);
		XMStoreFloat4x4(&constantBufferData->W_inverseTranspose, W_inverseTranspose);

		constantBufferData->material = (*materials)[i % materials->size()];

		// enable gpu access
		m_d3dContext->Unmap((*constantBuffers)[i].Get(), 0);
	}
}

void LightsDemoApp::UpdatePerFrameConstantBuffer() {
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	// PerFrameCB
	// disable gpu access
	XTEST_D3D_CHECK(m_d3dContext->Map(m_vsConstantBufferPerFrame.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
	PerFrameCB* constantBufferData = static_cast<PerFrameCB*>(mappedResource.pData);

	//update the data
	constantBufferData->dirLight = dirL;
	constantBufferData->pointLight = pointL;
	constantBufferData->spotLight = spotL;
	constantBufferData->eyePosW = m_camera.GetPosition();

	// enable gpu access
	m_d3dContext->Unmap(m_vsConstantBufferPerFrame.Get(), 0);
}

void LightsDemoApp::UpdateRarelyChangedConstantBuffer()
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	// PerFrameCB
	// disable gpu access
	XTEST_D3D_CHECK(m_d3dContext->Map(m_vsConstantBufferRarelyChanged.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
	RarelyChangedCB* constantBufferData = static_cast<RarelyChangedCB*>(mappedResource.pData);

	constantBufferData->useDirLight = useDirLight ? 1 : 0;
	constantBufferData->usePointLight = usePointLight ? 1 : 0;
	constantBufferData->useSpotLight = useSpotLight ? 1 : 0;

	// enable gpu access
	m_d3dContext->Unmap(m_vsConstantBufferRarelyChanged.Get(), 0);
}

#pragma endregion

#pragma region Mouse and Keyboard interface methods

void LightsDemoApp::OnWheelScroll(input::ScrollStatus scroll)
{
	// zoom in or out when the scroll wheel is used
	if (service::Locator::GetMouse()->IsInClientArea())
	{
		m_camera.IncreaseRadiusBy(scroll.isScrollingUp ? -0.5f : 0.5f);
	}
}

void LightsDemoApp::OnMouseMove(const DirectX::XMINT2& movement, const DirectX::XMINT2& currentPos)
{
	XTEST_UNUSED_VAR(currentPos);

	input::Mouse* mouse = service::Locator::GetMouse();

	// rotate the camera position around the cube when the left button is pressed
	if (mouse->GetButtonStatus(input::MouseButton::left_button).isDown && mouse->IsInClientArea())
	{
		m_camera.RotateBy(math::ToRadians(movement.y * -0.25f), math::ToRadians(movement.x * 0.25f));
	}

	// pan the camera position when the right button is pressed
	if (mouse->GetButtonStatus(input::MouseButton::right_button).isDown && mouse->IsInClientArea())
	{
		XMFLOAT3 cameraX = m_camera.GetXAxis();
		XMFLOAT3 cameraY = m_camera.GetYAxis();

		// we should calculate the right amount of pan in screen space but for now this is good enough
		XMVECTOR xPanTranslation = XMVectorScale(XMLoadFloat3(&cameraX), float(-movement.x) * 0.01f);
		XMVECTOR yPanTranslation = XMVectorScale(XMLoadFloat3(&cameraY), float(movement.y) * 0.01f);

		XMFLOAT3 panTranslation;
		XMStoreFloat3(&panTranslation, XMVectorAdd(xPanTranslation, yPanTranslation));
		m_camera.TranslatePivotBy(panTranslation);
	}

}

void LightsDemoApp::OnKeyStatusChange(input::Key key, const input::KeyStatus& status)
{
	// re-frame the cube when F key is pressed
	if (key == input::Key::F && status.isDown)
	{
		m_camera.SetPivot({ 0.f, 0.f, 0.f });
	}

	if (key == input::Key::D && status.isDown)
	{
		updateActiveLights = true;
		useDirLight = !useDirLight;
	}

	if (key == input::Key::P && status.isDown)
	{
		updateActiveLights = true;
		usePointLight = !usePointLight;
	}

	if (key == input::Key::S && status.isDown)
	{
		updateActiveLights = true;
		useSpotLight = !useSpotLight;
	}
}

#pragma endregion